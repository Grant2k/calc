const PLUS = "+";
const MINUS = "-";
const MULTIPLY = "*";
const DIVIDE = "/";
const DEL = "del";
const EQUAL = "=";
const DECIMAL = ".";
let a = "";
let b = "";
let op = "";

$( document ).ready(function() {
     $(".onscreen").click(function() {
               let value = $(this).attr("value");
               let screen = $(".screen");
               let display = screen.text(); 

               let decimalDot = isDecimalDot(value);
               let validOperator = isValidOperator(value);

               if (value === EQUAL) {
                    toCalculate();
                    return;
               }

               if (value === DEL) {
                    clear();
                    return;
               }

               if (decimalDot && !op && !isInt(a)) {
                    return;
               }

               if (decimalDot && op && !isInt(b)) {
                    return;
               }

               if (display == "0") {
                    screen.empty();
                    if (decimalDot) {
                    value = `0${value}`;
                    }
               }
               
               // if (!b && value == DECIMAL) {
               //      value = `0${value}`;
               // }

               screen.append(value); 

               //scale($('.screen')[0]);
               

               if (b && validOperator) {
                    toCalculate(); 
                    screen.append(value); 
               }
               
               if (validOperator) {
                    op = value;
               } else if (a && op) {
                    b += value;
               } else {
                    a = a == "0" ? value : `${a}${value}`;
               }

               // if (validOperator) {
               //      op = value;
               // } else if (a && op) {
               //      b = isDecimalDot(value) ? value : `${b}${value}`;
               // } else {
               //      a = a == "0" ? value : `${a}${value}`;
               // }   

               $("#scale").quickfit({max: 60, min: 8}); 
               
               console.log("f= " + isValidOperator());
               console.log("display " + screen.text());     
               console.log("value " + value);
               console.log ("a = " + a);  
               console.log ("b = " + b);      
               console.log ("op = " + op); 
          });

     $(function(){
          $("#rsz").resizable();
     });
     
     clear();
});
