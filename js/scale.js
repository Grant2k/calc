function scale(block) {
     var step = 0.1,
         minfs = 9,
         scw = block.scrollHeight,
         w = block.offsetHeight;
     if (scw < w) {
       var fontsize = parseInt($(block).css('font-size'), 10) - step;
       if (fontsize >= minfs){
         $(block).css('font-size', fontsize);
         scale(block);
       }
     }
}